<?php

namespace Tests\App\Unit\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testShouldDisplayHomepage(): void
    {
        $this->client = static::createClient();

        $this->client->request('GET', '/login');

        $this->assertSelectorTextContains('button', 'Se connecter');
    }
}

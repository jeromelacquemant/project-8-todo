<?php

namespace App\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TaskVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, ['TASK_EDIT', 'TASK_DELETE', 'TASK_DELETE_ANONYMOUS'])
            && $subject instanceof \App\Entity\Task;
    }

    protected function voteOnAttribute($attribute, $task, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        if(null == $task->getUser())
        {
            return false;
        }
        // ... (check conditions and return true to grant permission) ...
        
        switch ($attribute) {
            case 'TASK_DELETE':
                if($task->getUser()->getUsername() == "Anonymous") {
                    if($this->security->isGranted('ROLE_ADMIN')) {
                        return true;
                        break;
                    }
                }

                if ($task->getUser()->getId() == $user->getId()) {  
                    return true;
                    break;
                }
            return false;
            break;
        }
    }
}
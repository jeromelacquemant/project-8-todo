<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

/**
 * @codeCoverageIgnore
**/
class UserFixtures extends Fixture
{
    public const ROLE_USER = 5;
    public const ROLE_ADMIN = 11;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user 
            ->setUsername("Anonymous")
            ->setPassword(password_hash('anonimous', PASSWORD_BCRYPT))
            ->setEmail("anonymous@gmail.com");

        $manager->persist($user);
        $this->addReference('Anonymous', $user);
        $manager->flush();

        for($i = 1; $i<self::ROLE_USER; ++$i)
        {
            $user = new User();
            $user 
                ->setUsername("User".$i)
                ->setPassword(password_hash('password'.$i, PASSWORD_BCRYPT))
                ->setEmail("user_".$i."@gmail.com");
    
            $manager->persist($user);
            $this->addReference('User'.$i, $user);
        }
        $manager->flush();

        for($i = 5; $i<self::ROLE_ADMIN; ++$i)
        {
            $user = new User();
            $user 
                ->setUsername("User".$i)
                ->setPassword(password_hash('password'.$i, PASSWORD_BCRYPT))
                ->setEmail("user_".$i."@gmail.com")
                ->setRoles(['ROLE_ADMIN']);
    
            $manager->persist($user);
            $this->addReference('User'.$i, $user);
        }
        $manager->flush();
    }
}
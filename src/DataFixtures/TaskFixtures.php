<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\DataFixtures\UserFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * @codeCoverageIgnore
**/
class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public const NUMBER_DONE = 5;
    public const NUMBER_NOT_DONE = 11;
    public const NUMBER_ANY_USER = 15;

    public function load(ObjectManager $manager)
    {
        for($i = 1; $i<self::NUMBER_DONE; ++$i)
        {
            $task = new Task();
            $task 
                ->setTitle("Task n°".$i)
                ->setContent("Content of the task n°".$i)
                ->setIsDone(1)
                ->setUser($this->getReference('User'.mt_rand(1, 3)));
    
            $manager->persist($task);
        }

        for($i = 5; $i<self::NUMBER_NOT_DONE; ++$i)
        {
            $task = new Task();
            $task 
                ->setTitle("Task n°".$i)
                ->setContent("Content of the task n°".$i)
                ->setIsDone(0)
                ->setUser($this->getReference('User'.mt_rand(1, 3)));
    
            $manager->persist($task);
        }
        $manager->flush();

        for($i = 11; $i<self::NUMBER_ANY_USER; ++$i)
        {
            $task = new Task();
            $task 
                ->setTitle("Task n°".$i)
                ->setContent("Content of the task n°".$i)
                ->setIsDone(0)
                ->setUser($this->getReference("Anonymous"));
    
            $manager->persist($task);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
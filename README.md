TODO & CO (OpenClassrooms project)
========

# FUNCTIONALITIES
With this application, you can as a normal user :
- Consult the list of all tasks
- Add a new task linked to a user
- Modify a task
- Delete a task
- Mark a task as finished or in progress


With this application, you can as an admin :
- Consult the list of users
- Add a new user
- Modify the role of a user 


# INSTALLATION
clone or download the project : 
cd project-8-todo/ 
git clone https://gitlab.com/jeromelacquemant/project-8-todo.git

Update the .env with your database credentials.

Use Composer to install dependecies : 
**composer install**

Create the database : 
**php bin/console do:da:cr**

Create associated tables : 
**php bin/console do:sc:up --force**

Use the fixtures : 
**php bin/console do:fi:lo --append**

Lauch the server : 
**php bin/console se:ru (or symfony serve)**

# ACCESS AS A USER
To access the application, you need to be authenticated as a user. 
To test it this is a fixture you can used :
- username: **User1**
- password: **password1**

# ACCESS AS AN ADMIN
To access the application, you can be authenticated as an admin. 
To test it this is a fixture you can used :
- username: **User6**
- password: **password6**

DEVELOPPED WITH :
- Php - 7.4.24
- Symfony - 4.4.25
- VisualStudioCode - 2019.1.1 

AUTHOR
Jérôme LACQUEMANT
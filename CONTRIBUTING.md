# Introduction
This document explains how all developers wishing to make 
changes to the project should proceed. It will detail the 
quality process to use as well as the rules to follow. 
The goal is to have a project that several people can work 
on at the same time and easily.

While README files help people use the project, contribution 
documents help people contribute to the project. It explains 
what types of contributions are needed and how the process works.

We will see in particular how to work together on Gitlab. 

# General rules
## Teamwork
Teamwork on a development project is essential. 
Several rules are foreseen:
- Realization of the "daily" at 9:30 am every day 
(15 min meeting where the whole team quickly presents. 
What he has done and what he has planned to do, possibly 
the blocking points)
- Using the company's dedicated Slack channel.
- Meeting every Friday at 3 p.m. to discuss the important 
points of the week
- Using Trello software to distribute to-dos
- Using Gitlab
- Code Editor Preference : VisualStudioCode

## Ask about
You arrive on the project, it's great! In order not to get 
lost, you have to start at the beginning. Here are some 
rules to follow.

1. Browse the README file of the project
2. Browse the exits (open and closed)
3. Watch the commits activity on the main branch.
4. Look at the code as a whole to get an overview
5. Before asking for help, first check if the problem 
has not been discussed on the project
6. Learn how to do the project (use indentations, semicolons 
or comments differently from what you would do in your own repository)

**Important point**
It's OK not to know everything, but show that you've tried. 
Before asking for help, be sure to check the README file, 
documentation, issues (open or closed), and internet research 
for an answer. The team will appreciate it when you demonstrate 
that you are trying to learn on your own first.

## Start contributing#
For a project, there are 2 types of contributions: issues 
and pull requests. Let's see what the difference is.

### Open an issue
Issues are like starting a conversation or a discussion.

Opening an exit is recommended in the following situations:
- Report an error that you cannot resolve yourself
- Discuss a topic or a new idea
- Suggest a new feature

Tips for communicating about issues
If you see an open issue that you want to tackle, comment on 
the issue to let others know you're working on it. This way 
people will be less likely to duplicate your work.

If an issue was opened a while ago, it may be addressed elsewhere 
or may have already been resolved, so comment to ask for 
confirmation before starting work.

If you opened an issue, but found the answer later, comment on 
the issue to let people know, then close it. Even documenting 
this result is a contribution to the project.

### Open a pull request
Pull Request (PR) are there to start working on a solution.

Opening a pull request is recommended in the following situations:
- Submit trivial corrections (for example, a typo, broken 
link, or obvious error)
- Start working on a contribution that has already been requested, 
or that you have already discussed in an issue

A pull request doesn't have to represent finished work. 
It's usually best to open a pull request early so others 
can watch or give feedback on your progress. Just mark “WIP” 
(Work in Progress) in the subject line. You can always add more 
commits later.

## Testing modifications
Testing your changes is an essential point in a project. 
Perform your changes against existing tests if they exist 
and create new ones if necessary. Whether the tests exist 
or not, make sure your changes don't break the existing project.

## Discovery of an error
In the case of the discovery of an error, the context must 
be given. Explain what you are trying to do and how to 
reproduce it. If you come up with a new idea, explain why 
you think it would be useful for the project (not just for you).

## Comment and commit
Comment on your code on what seems essential. You should 
not put comments everywhere but put them only when necessary. 
Likewise, commits should be easy to understand.

# Working with Gitlab
After this general introduction to good practices, 
let's get into the technical and concrete side. 
How are we going to do this?

All project monitoring is done here on Gitlab:
https://gitlab.com/jeromelacquemant/project-8-todo

## Advantages
Working with Gitlab has several advantages, including:
- Manage multiple git repositories, making it easier 
for people to work together
- Web interface to interact with the git repository
- Management of “pull request” or “merge request”
- Continuous integration approach
- “Online” filing accessible by all

## Rules
We will always work with 2 deposits:
- The local depot
- A “remote” repository with a name associated with it

## Ready ? Develop !
To start, clone the project from Gitlab locally: 
**“git clone”**.

At least every day, get the latest version available on the server: 
**“git pull”**.
This allows you to integrate the latest version available 
on the server into your work, and therefore to take into 
account the work of others.

Create a new branch for making your changes: 
**"git branch"**

For every important action, make a commit: 
**“git commit”**

Test your modifications locally using the tests found in the :
**“src / tests”**

To push your work to the remote repository, use the command: 
**“git push”**.
The current state of the current branch (and its history) 
is then transferred to the remote repository. Note, unit 
and functional tests are present. They are directly related 
to the continuous integration of Gitlab. So, as soon as you 
push a branch on the repository, the tests will start.

Namely, it is forbidden to directly merge your branch into 
the master branch without a review of your branch by another 
developer. In order to notify other developers of your work, 
you will need to create a merge request on Gitlab. This is a 
request to integrate your work. If the developer doing the 
review validates, then the branch will be merged into the master branch.

**You are now ready to start working on the project!**




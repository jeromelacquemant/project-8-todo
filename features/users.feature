@userstest

Feature: Users
  In order to manage the users on the application
  As a admin
  I need to be able to create/list/edit them

@loginAsAdminFabrice
  Scenario: List the users 
    Given I am on "/users"
    Then I should see 12 user
    And I should see "User1"
    And I should see "user_1@gmail.com"

@loginAsAdminFabrice
  Scenario: Create a user 
    Given I am on "/users/create"
    When I fill in "user_username" with "Thierry"
    And I fill in "user_password_first" with "passwordtest"
    And I fill in "user_password_second" with "passwordtest"
    And I fill in "user_email" with "thierry@email.com"
    And I select "ROLE_USER" from "user_roles_0"
    And I press "Ajouter"
    Then I should be on "/users"
    And I should see "L'utilisateur a bien été ajouté"
    And I should see "Thierry"
    And I should see "thierry@email.com"

@loginAsAdminFabrice
  Scenario: Create an admin 
    Given I am on "/users/create"
    When I fill in "user_username" with "Philippe"
    And I fill in "user_password_first" with "passwordtest"
    And I fill in "user_password_second" with "passwordtest"
    And I fill in "user_email" with "philippe@email.com"
    And I select "ROLE_ADMIN" from "user_roles_1"
    And I press "Ajouter"
    Then I should be on "/users"
    And I should see "L'utilisateur a bien été ajouté"
    And I should see "Philippe"
    And I should see "philippe@email.com"

@loginAsAdminFabrice
  Scenario: Edit a user to an admin
    Given I am on "/users/1/edit"
    When I fill in "user_username" with "Thierry"
    And I fill in "user_email" with "thierry@gmail.com"
    And I fill in "user_password_first" with "passwordtest"
    And I fill in "user_password_second" with "passwordtest"
    And I select "ROLE_ADMIN" from "user_roles_1"
    And I press "Modifier"
    Then I should be on "/users"
    And I should see "L'utilisateur a bien été modifié"
    And I should see "Thierry"
    And I should see "thierry@gmail.com"

@loginAsAdminFabrice
  Scenario: Edit an admin to a user
    Given I am on "/users/1/edit"
    When I fill in "user_username" with "Philippe"
    And I fill in "user_email" with "philippe@gmail.com"
    And I fill in "user_password_first" with "passwordtest"
    And I fill in "user_password_second" with "passwordtest"
    And I select "ROLE_USER" from "user_roles_0"
    And I press "Modifier"
    Then I should be on "/users"
    And I should see "L'utilisateur a bien été modifié"
    And I should see "Philippe"
    And I should see "Philippe@gmail.com"

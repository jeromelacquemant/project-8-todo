@authorizationtest

Feature: Authorization
  In order to access some resource on the application
  As a user
  I need to be logged in
  I need to have a granted access

  Scenario: Access public pages as anonymous user
    When I go to "/login"
    Then the response status code should be 200
    And I should be on "/login"


  Scenario Outline: Access protected pages as anonymous user
    When I go to "<url>"
    And I should be on "/login"

    Examples:
      | url           |
      | /             |
      | /tasks      |


  @loginAsUserDaniel
  Scenario Outline: Access protected pages about tasks as logged user
    When I go to "<url>"
    Then the response status code should be 200
    And I should be on "<url>"

    Examples:
      | url           |
      | /tasks        |
      | /tasks/create |


  @loginAsUserDaniel
  Scenario Outline: Access protected pages about users as loggeduser
    When I go to "<url>"
    Then the response status code should be 403
    And I should be on "<url>"

    Examples:
      | url           |
      | /users      |
      | /users/create      |

  @loginAsAdminFabrice
  Scenario Outline: Access protected pages about tasks as logged user
    When I go to "<url>"
    Then the response status code should be 200
    And I should be on "<url>"

    Examples:
      | url           |
      | /             |
      | /tasks        |
      | /tasks/create |


  @loginAsAdminFabrice
  Scenario Outline: Access protected pages about users as loggeduser
    When I go to "<url>"
    Then the response status code should be 200
    And I should be on "<url>"

    Examples:
      | url           |
      | /             |
      | /users      |
      | /users/create      |
@taskstest

Feature: Tasks
  In order to manage the tasks on the application
  As a user
  I need to be able to create/view/edit/delete/mark tasks

  @loginAsUserDaniel
  Scenario: List tasks
    Given I am on "/tasks"
    And I should see 14 tasks

  @loginAsUserDaniel
  Scenario: Create a task
    Given I am on "/tasks/create"
    And I should see 0 tasks
    When I fill in "task_title" with "Titre de la tâche"
    And I fill in "task_content" with "Contenu de la tâche"
    And I press "Ajouter"
    Then I should be on "/tasks"
    And I should see "La tâche a été bien été ajoutée."
    And I should see 15 tasks
    And I should see "Titre de la tâche"
    And I should see "Contenu de la tâche"

  Scenario: Delete a task
    Given I am on "/login"
    When I fill in "username" with "User2"
    And I fill in "password" with "password2"
    And I press "Se connecter"
    Then I should be on "/"
    Given I am on "/tasks"
    And I should see 14 tasks
    When I press "Supprimer"
    And I should see 13 tasks
    And I should not see "Tache 1"

  @loginAsUserDaniel
  Scenario: Mark as done one task
    Given I am on "/tasks"
    And I should see 14 tasks
    And I should see 10 tasks to do
    When I press "Marquer comme faite"
    And I should see 9 tasks to do

  @loginAsUserDaniel
  Scenario: Mark as undone one task
    Given I am on "/tasks"
    And I should see 14 tasks
    And I should see 4 done tasks
    When I press "Marquer non terminée"
    And I should see 3 done tasks
@authenticationtest

Feature: Authentication
  In order to authenticate on the application
  As a user
  I need to be able to login/logout

  Scenario: Login
    Given I am on "/login"
    When I fill in "username" with "User1"
    And I fill in "password" with "password1"
    And I press "Se connecter"
    Then I should be on "/"

  Scenario: Throw error message when the user authentication failed
    Given I am on "/login"
    When I fill in "username" with "False user"
    And I fill in "password" with "Bad password"
    And I press "Se connecter"
    Then I should be on "/login"
    And I should see "Invalid credentials"
  
  @loginAsUserDaniel
  Scenario: Logout
    Given I am on "/"
    When I click "Se déconnecter"
    Then I should be on "/login"

  @loginAsAdminFabrice
  Scenario: Logout
    Given I am on "/"
    When I click "Se déconnecter"
    Then I should be on "/login"



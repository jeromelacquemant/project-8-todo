<?php

use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Persistence\ManagerRegistry;
use Behat\MinkExtension\Context\MinkContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{
    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @var array
     */
    private $classes;

    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var User
     */
    private $currentUser;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     * @param ManagerRegistry              $doctrine
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(ManagerRegistry $doctrine, UserPasswordEncoderInterface $encoder, ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->em = $entityManager;

        $manager = $doctrine->getManager();
        $this->schemaTool = new SchemaTool($manager);
        $this->classes = $manager->getMetadataFactory()->getAllMetadata();
        $this->doctrine = $doctrine;
        $this->encoder = $encoder;

        $this->setupDatabase();
        $this->loadFixtures();
    }

    protected function setupDatabase()
    {
        $metaData = $this->em->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($this->em);
        $schemaTool->dropDatabase();
        if (!empty($metaData)) {
            $schemaTool->createSchema($metaData);
        }
    }

    protected function loadFixtures()
    {
        $purger = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);

        $taskFixtures = new TaskFixtures();
        $userFixtures = new UserFixtures();

        $loader = new Loader();
        $loader->addFixture($taskFixtures);
        $loader->addFixture($userFixtures);

        $executor->execute($loader->getFixtures());
    }

    /**
     * @BeforeScenario @loginAsUserDaniel
     */
    public function iAmLoggedInAsUser()
    {
        $user = new User();
        $user->setUsername('Daniel');
        $user->setPassword($this->encoder->encodePassword($user, 'passworduser'));
        $user->setEmail('daniel@gmail.com');
        $user->setRoles(['ROLE_USER']);

        $this->currentUser = $user;

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        $this->visitPath('/login');
        $this->fillField('username', 'Daniel');
        $this->fillField('password', 'passworduser');
        $this->pressButton('Se connecter');
    }

    /**
     * @BeforeScenario @loginAsAdminFabrice
     */
    public function iAmLoggedInAsAdmin()
    {
        $user = new User();
        $user->setUsername('Fabrice');
        $user->setPassword($this->encoder->encodePassword($user, 'passwordadmin'));
        $user->setEmail('fabrice@gmail.com');
        $user->setRoles(['ROLE_ADMIN']);

        $this->currentUser = $user;

        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();

        $this->visitPath('/login');
        $this->fillField('username', 'Fabrice');
        $this->fillField('password', 'passwordadmin');
        $this->pressButton('Se connecter');
    }

    /**
     * @When /^(?:|I )click "(?P<link>(?:[^"]|\\")*)"$/
     *
     * @param $link
     */
    public function iClick($link)
    {
        $this->clickLink($link);
    }

    /**
     * @Then /^(?:|I )should see (?P<num>\d+) users?$/
     *
     * @param $num
     *
     * @throws \Behat\Mink\Exception\ExpectationException
     */
    public function iShouldSeeUsers($num)
    {
        $this->assertSession()->elementsCount('css', 'tbody tr', (int) $num);
    }

    /**
     * @Then /^(?:|I )should see (?P<num>\d+) tasks?$/
     *
     * @param $num
     *
     * @throws \Behat\Mink\Exception\ExpectationException
     */
    public function iShouldSeeTasks($num)
    {
        $this->assertSession()->elementsCount('css', '.thumbnail', (int) $num);
    }

    /**
     * @Then /^(?:|I )should see (?P<num>\d+) tasks to do?$/
     *
     * @param $num
     *
     * @throws \Behat\Mink\Exception\ExpectationException
     */
    public function iShouldSeeTasksToDo($num)
    {
        $this->assertSession()->elementsCount('css', '.glyphicon-remove', (int) $num);
    }

    /**
     * @Then /^(?:|I )should see (?P<num>\d+) done tasks?$/
     *
     * @param $num
     *
     * @throws \Behat\Mink\Exception\ExpectationException
     */
    public function iShouldSeeDoneTasks($num)
    {
        $this->assertSession()->elementsCount('css', '.glyphicon-ok', (int) $num);
    }
}
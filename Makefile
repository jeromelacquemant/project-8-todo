all:
	php bin/console d:d:d --force
	php bin/console d:d:c
	php bin/console d:m:m --no-interaction
	php bin/console make:migration
	php bin/console doctrine:fixtures:load --no-interaction
	vendor/bin/phpunit
	APP_ENV=test vendor/bin/behat
.PHONY: all

phpunit:
	vendor/bin/phpunit
.PHONY: phpunit

behat:
	APP_ENV=test vendor/bin/behat
.PHONY: behat

migration-doctrine:
	php bin/console d:d:d --force
	php bin/console d:d:c
	php bin/console d:m:m --no-interaction
	php bin/console make:migration
	php bin/console doctrine:fixtures:load --no-interaction
.PHONY: migration-doctrine